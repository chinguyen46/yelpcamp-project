var mongoose = require("mongoose");
var Campground = require("./models/campground.js");
var Comment = require("./models/comment.js");


var data = [
	{	name: "Cloud's Rest", 
		image:"https://www.nationalparks.nsw.gov.au/-/media/npws/images/parks/cattai-national-park/cattai-campground/cattai-campground-05.jpg", 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,"},
			
	{	name: "Stormy Hills", 
		image:"https://www.nationalparks.nsw.gov.au/-/media/npws/images/parks/cattai-national-park/cattai-campground/cattai-campground-05.jpg", 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,"},		
			
	{	name: "Still I Rise Top", 
		image:"https://www.nationalparks.nsw.gov.au/-/media/npws/images/parks/cattai-national-park/cattai-campground/cattai-campground-05.jpg", 
		description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,"},		
];

function seedDB(){
	//REMOVE CAMPGROUNDS
	Campground.deleteMany({}, function(err){
	if(err){
		console.log(err);
	}
		console.log("removed campgrounds");
			//CREATE CAMPGROUNDS
		data.forEach(function(seed){
		Campground.create(seed, function(err, campground){
			if(err){
				console.log(err);
			}else{
				console.log("added campground");
				//CREATE COMMENTS
				Comment.create(
					{
					 text: "This place is beautiful",
					 author: "Chi"
					}, function(err, comment){
						if(err){
							console.log(err);
						}else{
							campground.comments.push(comment);
							campground.save();
							console.log("Created new comment");
						}
					});
			}
		});
	});
	});
};

module.exports = seedDB; 
