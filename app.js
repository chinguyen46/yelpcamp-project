require('dotenv').config();
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var Campground = require("./models/campground.js");
var Comment = require("./models/comment.js");
var seedDB = require("./seeds.js");
var passport = require("passport");
var User = require("./models/user.js");
	LocalStrategy = require("passport-local");
	passportLocalMongoose = require("passport-local-mongoose");
var commentRoutes = require("./routes/comments.js");
var campgroundRoutes = require("./routes/campgrounds.js");
var indexRoutes = require("./routes/index.js");
var methodOverride = require("method-override");
var flash = require("connect-flash");
//use alternate localhost and the port Heroku assigns to $PORT
const host = '0.0.0.0';
const port = process.env.PORT || 3000;
//======================================================================================================

mongoose.connect(process.env.DATABASEURL, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true});

// mongoose.connect("mongodb://localhost/yelp_camp_v11", {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true});

				 
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/public"));
app.use(flash());



//PASSPORT CONFIG
app.use(require("express-session")({
	secret: "12345",
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
	passport.use(new LocalStrategy(User.authenticate()));
	passport.serializeUser(User.serializeUser());
	passport.deserializeUser(User.deserializeUser());

//Pass currentUser/messageVar to every single template
app.use(function (req, res, next){
	res.locals.currentUser = req.user;
	res.locals.errorMessageVar = req.flash("error");
	res.locals.successMessageVar = req.flash("success");
	next();
});

app.use(methodOverride("_method"));
app.use(indexRoutes);
app.use(campgroundRoutes);
app.use(commentRoutes);

//======================================================================================================


//======================================================================================================
app.listen(port, host, function() {
  console.log("Server started.......");
});