var express = require("express");
var router = express.Router();
var passport = require("passport");
var User = require("../models/user.js");


//HOME ROUTE
router.get("/", (req, res) =>{
	res.render("landing.ejs");
});

//AUTHENTICATION ROUTES

//REGISTER NEW USER FORM ROUTE
router.get("/register", (req, res) =>{
	res.render("authentication/register.ejs");
});
//POST NEW USER INPUT TO SERVER
router.post("/register", (req, res) =>{
	var newUser = new User({username: req.body.username});
	User.register(newUser, req.body.password, (err, newUser) =>{
		if(err){
			req.flash("error", err.message);
			res.redirect("/register");
		}else{
			passport.authenticate("local")(req, res, function() {
				req.flash("success", "Welcome to YelpCamp " + newUser.username);
				res.redirect("/campgrounds");
			});
		}
	});
});

//LOGIN ROUTES
//LOGIN PAGE FORM ROUTE
router.get("/login", (req, res) =>{
	res.render("authentication/login.ejs");
});
//POST USER LOGIN INPUT TO SERVER
router.post("/login", passport.authenticate("local", {
		successRedirect:"/campgrounds",
		failureRedirect:"/login"
		}), (req, res) =>{
			console.log("login check done");
		});

//LOGOUT ROUTES
//LOGOUT USER
router.get("/logout", (req, res) =>{
	req.logout();
	req.flash("success", "Succesfully logged out");
	res.redirect("/campgrounds");
});



module.exports = router; 

