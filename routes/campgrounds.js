var express = require("express");
var router = express.Router();
var Campground = require ("../models/campground.js");
var Comment = require ("../models/comment.js");
var middleware = require("../middleware/index.js");


//INDEX ROUTE - show all campgrounds
router.get("/campgrounds", (req, res) =>{
	Campground.find({}, function(err, campgrounds){
		if(err){
			console.log(err);
		}else{
			res.render("campgrounds/index.ejs", {campgroundsVar: campgrounds});
		}
	});
});

//CAMPGROUND ROUTE
//NEW ROUTE - show form to create new campground
router.get("/campgrounds/new", middleware.isUserLoggedIn, (req, res) =>{
	res.render("campgrounds/new.ejs");
});
//CREATE ROUTE - add new campground to db
router.post("/campgrounds", middleware.isUserLoggedIn, (req, res) =>{
	Campground.create(req.body.campground, function(err, newCampground){
		if(err){
			console.log(err);
		}else{
		//add logged in user id and username to campground object
		newCampground.author.id = req.user._id;
		newCampground.author.username = req.user.username;
		newCampground.save();
		res.redirect("/campgrounds"); //the default redirect is a GET request	
		}
	});
});
//SHOW ROUTE - shows more info about one campground
router.get("/campgrounds/:id", (req, res) =>{
Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground){
	if(err){
		console.log(err + " : No id match in database");
	}else{
		res.render("campgrounds/show.ejs", {foundCampgroundVar: foundCampground});
	}
});
});	


//EDIT ROUTE - edit information of specific campground 
router.get("/campgrounds/:id/edit", middleware.checkCampgroundOwnership, (req, res) =>{
	Campground.findById(req.params.id, function(err, foundCampground){
		if(err){
			req.flash("error", "Campground not found");
			res.redirect("/campgrounds");
		}else{
			res.render("campgrounds/edit.ejs", {foundCampgroundVar: foundCampground});
		}
	});	
});
		

//UPDATE ROUTE - update information of specific campground 
router.put("/campgrounds/:id", middleware.checkCampgroundOwnership, (req, res) =>{
	Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground){
		if(err){
			console.log(err);
		}else{
			res.redirect("/campgrounds/" + req.params.id);
		}
	});
});
//DELETE ROUTE - delete specific campground
router.delete("/campgrounds/:id", middleware.checkCampgroundOwnership, (req, res) =>{
  Campground.findByIdAndRemove(req.params.id, (err, campgroundRemoved) => {
        if(err) {
            res.redirect("/campgrounds");
       }
       		 Comment.deleteMany( {_id: { $in: campgroundRemoved.comments } }, (err) => {
            if (err) {
                console.log(err);
            }
            res.redirect("/campgrounds");
        });
    })
});




module.exports = router; 