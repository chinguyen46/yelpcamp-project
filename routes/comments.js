var express = require("express");
var router = express.Router();
var Campground = require ("../models/campground.js");
var Comment = require ("../models/comment.js");
var middleware = require("../middleware/index.js");


//NESTED RESTful COMMENT ROUTES
//CREATE ROUTE
router.get("/campgrounds/:id/comments/new", middleware.isUserLoggedIn, (req, res) =>{
	Campground.findById(req.params.id, function(err, foundCampground){
		if(err){
			res.redirect("/campgrounds");
		}else{
			res.render("comments/new.ejs", {foundCampgroundVar: foundCampground});
		}
	});
});

//UPDATE ROUTE
router.post("/campgrounds/:id/comments", middleware.isUserLoggedIn, (req, res) =>{
	Campground.findById(req.params.id, function(err, foundCampground){
		if(err){
			res.redirect("/campgrounds");
		}else{
			Comment.create(req.body.comment, function(err, newComment){
				if(err){
					req.flash("error", "Comment not found");
					console.log(err);
				}else{
					//create new comment
					newComment.author.id = req.user._id;
					newComment.author.username = req.user.username;
					newComment.save();
					//add comment to chosen campground page
					foundCampground.comments.push(newComment);
					foundCampground.save();
					req.flash("success", "Comment added");
					res.redirect("/campgrounds/" + req.params.id);
				}
			});
		}
	});
});

//EDIT COMMENT ROUTE
router.get("/campgrounds/:id/comments/:comment_id/edit", middleware.checkCommentOwnership, (req, res) =>{
	Comment.findById(req.params.comment_id, function(err, foundComment){
		if(err){
			res.redirect("back");
		}else{
			res.render("comments/edit.ejs", {campground_id: req.params.id, foundCommentVar: foundComment});
		}
	});
});
//UPDATE COMMENT ROUTE
router.put("/campgrounds/:id/comments/:comment_id", middleware.checkCommentOwnership, (req, res) =>{
	Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, updatedComment){
		if(err){
			console.log(err);
		}else{
			res.redirect("/campgrounds/" + req.params.id);
		}
	});
});
//DELETE COMMENT ROUTE
router.delete("/campgrounds/:id/comments/:comment_id", middleware.checkCommentOwnership, (req, res) =>{
	Comment.findByIdAndRemove(req.params.comment_id, function(err){
		if(err){
			res.redirect("back");
		}else{
			req.flash("success", "Comment deleted");
			res.redirect("/campgrounds/" + req.params.id);
		}
	});
});






module.exports = router; 